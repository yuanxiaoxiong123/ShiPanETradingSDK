#ShiPanETradingSDK
### Introduction
Attention: This SDK not supports only ShiPanE 3.4.10 or higher due to the API change.

This is the SDK designed to wrap ShiPanE APIs in C#.

ShiPanE is a stock trading api that supporting merely all stock trading client made upon TDX published on  "http://www.iguuu.com/e". You can go to this webpage to download and try it.Currently it is free to use as long as you regist at their website. I am not related to them and I am sololy a user.

ShiPane itself provides RESTFul api for you to use. This program is a wrapper that enables you to use it directly in C#.

This is a project written in Visual Studio 2015 Community. You can just open it and add it as a reference in your project.

This project depends on NLog, Newtonsoft.Json and Restclient. You don't need to worry about these since NuGet will help you with all the dependecies.

Comments & bug reports are welcome.

Note: I do speak Chinese as you can see in code comments, no worries, since I am a native born Chinese.

### Usage
Usage is kept as simple as possible,easy as 1,2,3

YBO is my nick name, no real meaning in it.

1st Step:
Create a ShiPanETrader Object
```
using YBOStockTrader;
ShiPanETrader mytrader = new ShiPanETrader();
```

2ed Step:
Initialize it with host info by calling `SetTdxwHost()`

```
mytrader.SetTdwxHost("Address_To_ShiPanE_Host",[optional]Port Number);
```
Address_To_ShiPanE_Host:The Ip address of the machine that runs ShiPanE;
Port Number: ShiPanE port number.This parameter is optional.If nothing is set,default value 8888 will be used.

3rd Step:
Run your command.
This SDK provides all the basic method you need to do your daily tradings.All the method are syncronized. Async support is not a main issue for me since the implementation will get much more complicated.

Here, we provide following method:
**1.** Query current orders:`GetOrders()`
```
mytrader.GetOrders();
```
This method will return all the orders placed in a list of `ShiPanEOrderStatusRespond` struct.

**2.** Query completed orders:`GetCompletedOrders()`
```
mytrader.GetCompletedOrders();
```
This method will return all completed(filled) orders placed in a list of `ShiPanEOrderStatusRespond` struct.

**3.** Query cancelable orders:`GetCancelableOrders()`
```
mytrader.GetCancelableOrders();
```
This method will return all cancelable(unfilled) orders placed in a list of `ShiPanEOrderStatusRespond` struct.

**4.** Query positions:`GetPositions()`
```
mytrader.GetPositions();
```
This method will return all current postion in a list of `ShiPanEStockPosition`.In the meantime, capital info described by object member `mytrader.AccountAsset` is updated.

**5.** Place a order:`PlaceOrder(string label,string BuyOrSell,ShiPanEOrderPriceType OrderPriceType,float price,int amount)`
```
mytrader.PlaceOrder("600256","BUY",12.56,1000);
```
The example will place a order that buy 1000 shares of 600256 at 12.56Yuan/share and return the order number of it if success. -1 is returned if error occures.
label: 6 digits stock label in string format.
BuyOrSell:Place "BUY" to buy and place "SELL" to sell.Upper and lower case both work.
OrderPriceType: price type of the order. Currently 7 types are supported as listed in `enum ShiPanEOrderPriceType`
price:self-explainary.
amount:Stock share.1000 = 1000 shares,not 1000*100.

**6.** Cancel a order:`CancelOrder(int Order_Number)`
```
mytrader.CancelOrder(Order_Number)
```
Cancel the order with order number `Order_Number`.This number can be obtained by the return value of `PlaceOrder()` or from the `OrderNum` member of `ShiPanEStockPosition`.

**7.** Cancel all order:`CancelOrders()`
```
mytrader.CancelOrders()
```
Cancel all orders cancelable.

**8.** Obtain new stock list for a certain date`GetNewStockList(DateTime date)`
```cs
mytrader.GetNewStockList(new DateTime(year,month,day))
```
Obtain the new stock list and return them in an array of string[].Only date part of `date` will be used. Time part will be ignored.
Data is downloaded from SINA. Sometimes network does go nuts, you might want to try it multiple times to ensure consistent result.
Don't go to hard, tho. You will get banned if you request the webpage too frequently.

**9.** Obtain new stock list for today`GetNewStockListToday()`
```cs
mytrader.GetNewStockListToday()
```
Just a wrapper of `GetNewStockList(DateTime.Now)`

## Full Demo

Since this is a class library, You will need to use it in a new project.Hereby I created a new C# Console project called "ShiPanETraderSDKDemo".

First,add my project to your solution.Second,add my project in your reference,then try the following code.

If you're running ShipanE and this project on different machines, you should replace 127.0.0.1 with the IP of the machine that ShiPanE runs on.

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShiPanEStockTrader;
namespace ShiPanETraderSDKDemo
{
    class Program
    {    
        static void Main(string[] args)
        {
            ShiPanETrader mytrader = new ShiPanETrader();

            //Use the IP address of the machine running ShiPanE here.localhost or 127.0.0.1 means the program runs on the same machine that runs ShiPanE
            int init=mytrader.SetTdxwHost("127.0.0.1");
            if (init != 0)
            {
                Console.WriteLine("Initialize ShiPanETrader failed.Exit.");
                return;
            }
            //Obtain Orders
            List<ShiPanEOrderStatusRespond> OrderResponses = mytrader.GetOrders();
            Console.WriteLine("Currently I have {0} orders.", OrderResponses.Count);

            //Place a order
            int Order_Num = mytrader.PlaceOrder("600256", "BUY", ShiPanEOrderPriceType.Limit, 12.56, 1000);
            if (Order_Num > 0)
                Console.WriteLine("Order placed. Order number is {0}", Order_Num);
            else
                Console.WriteLine("Placing order failed.Please check again.");

            //Obtain Positions
            List<ShiPanEStockPosition> AccountPositions = mytrader.GetPositions();
            Console.WriteLine("Currently I have {0} positions.", AccountPositions.Count);

            //Obtain Usable Capital
            Console.WriteLine("My usable capital is {0}.",mytrader.AccountAsset.CanUse);


            //Finish line
            Console.WriteLine("Program finished.");
            Console.ReadLine();
        }
    }
}
```




### 简介
注意：因为实盘易APi的变化，现不再提供对实盘易3.4.10.0以前的版本提供支持。

本程序实现的是实盘易在C#的调用API。

实盘易是一个在通达信软件终端模拟操作的软件，发布人为http://www.iguuu.com/e. 你可以前往他们的页面去下载。当前软件的使用的是免费的，唯一的需求是在网站上注册一个账号。我本人不是实盘易的开发人，只是一名用户。我本人与实盘易没有任何实质的联系。

实盘易提供了一系列RESTFul api供你使用.本程序能让你C#中直接调用所有的接口.

本程序用Visual Studio 2015社区版开发。你可以直接下载本程序然后在你自己的程序中添加引用。

本程序依赖于NLog, Newtonsoft.Json and Restclient.基本上你不需要操心这些依赖，NuGet会处理好这些问题。

欢迎提供评论和Bug反馈。

注意：中英文对于我来说区别不是很大，你可以用你觉得方便的语言和我交流。我本人是中国人，虽然这里的中文看起来比较别扭。

### 用法
该程序的设计的原则是尽量简单，正常的使用只需要三步。

YBO在这里是我的绰号，没有任何的实际意义。因为这个项目一开始是我本人使用，所以命名上没有特别注意。

第一步:
实例化ShiPanETrader对象
```
using YBOStockTrader;
ShiPanETrader mytrader = new ShiPanETrader();
```

第二步:
调用`SetTdxwHost()`对该对象进行初始化

```
mytrader.SetTdwxHost("Address_To_ShiPanE_Host",[optional]Port Number);
```
Address_To_ShiPanE_Host:运行实盘易的机器的IP;
Port Number: 实盘易使用的端口号。这是一个可选选项，如果没有填写，则会使用默认的8888端口。

第三部:
运行命令。
本程序提供了所有的日常交易所需功能。所有的method都是同步调用的。异步调用涉及到界面的锁问题，过于复杂。

本程序提供了一下方法
**1.** 获取当前所有委托:`GetOrders()`
```
mytrader.GetOrders();
```
该方法会获取所有的委托，返回一个包含 `ShiPanEOrderStatusRespond` 结构的列表(List<>).

**2.** 获取当前所有成交委托:`GetCompletedOrders()`
```
mytrader.GetCompletedOrders();
```
该方法会获取所有成交的委托，返回一个包含 `ShiPanEOrderStatusRespond` 结构的列表(List<>).

**3.**  获取当前所有可撤销的委托:`GetCancelableOrders()`
```
mytrader.GetCancelableOrders();
```
该方法会获取所有可撤销的委托，返回一个包含 `ShiPanEOrderStatusRespond` 结构的列表(List<>).

**4.** 获取仓位:`GetPositions()`
```
mytrader.GetPositions();
```
该方法会获取当前账户的持仓，返回一个包含`ShiPanEStockPosition`结构的列表(List<>).同时更新包含账户资金信息的成员 `mytrader.AccountAsset`.

**5.** 下单:`PlaceOrder(string label,string BuyOrSell,ShiPanEOrderPriceType OrderPriceType, float price,int amount)`
```
mytrader.PlaceOrder("600256","BUY",12.56,1000);
```
这个实例会以12.56元的价格下单购买1000股600256，并返回订单号。如果下单过程出错，则会返回-1.
label: 股票的六位数代码
BuyOrSell:"BUY" 代表买入， "SELL"代表卖出。大小写不敏感。
OrderPriceType:委托方式。现在一共支持7种委托方式，详情请见`enum ShiPanEOrderPriceType`
price:委托价格
amount:委托数量。这是股票数量，不是手数。

**6.** 撤单:`CancelOrder(int Order_Number)`
```
mytrader.CancelOrder(Order_Number)
```
撤销订单号为Order_Num的订单。订单号可以从 `PlaceOrder()` 返回值或者`ShiPanEStockPosition`中的`OrderNum`成员处获得。

**7.** 撤销所有委托:`CancelOrders()`
```
mytrader.CancelOrders()
```
撤销所有可以撤销的委托。

**8.** 查询某日可申购的新股`GetNewStockList(DateTime date)`
```cs
mytrader.GetNewStockList(new DateTime(year,month,day))
```
查询date所代表的日期里可申购的新股，返回一个包含可申购的新股代码的string[]。date中只有日期会被调用，具体时间部分会被忽略。
数据是从新浪的新股列表页面获取的。这是一个非常基本的实现，没有加入可靠性的检查。如果你希望保证结果不会因为网络波动受到影响，你最好自己加入重试的机制。
重试的频率由你自己决定，要控制频率，高频的请求可能会导致你被新浪拉入黑名单。

**9.** 返回当日可申购新股`GetNewStockListToday()`
```cs
mytrader.GetNewStockListToday()
```
返回调用当日可申购新股的列表，实际为`GetNewStockList(DateTime.Now)`


## 完整实例

因为这是一个类库，所以你需要在一个新的项目中引用它。这里我们新创建一个C#控制台项目，叫 "ShiPanETraderSDKDemo"。

下面有两个准备工作要做，第一，将我的类库工程添加到"ShiPanETraderSDKDemo"解决方案，第二，在"ShiPanETraderSDKDemo"中添加对该工程的引用。

然后你就可以尝试运行以下代码。如果你的实盘易配置正常，你应该能够运行以下代码。

如果你的实盘易运行的机器和调试改程序的机器是分开的，那么请自行将SetTdxwHost的IP地址替换为运行实盘易的机器的IP地址。

```cs
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShiPanEStockTrader;
namespace ShiPanETraderSDKDemo
{
    class Program
    {    
        static void Main(string[] args)
        {
            ShiPanETrader mytrader = new ShiPanETrader();

            //如果你的实盘易运行的机器和调试改程序的机器是分开的，那么请自行将SetTdxwHost的IP地址替换为运行实盘易的机器的IP地址
            int init=mytrader.SetTdxwHost("127.0.0.1");
            if (init != 0)
            {
                Console.WriteLine("实盘易初始化失败，请检查设置。");
                return;
            }
            //获取所有当日委托
            List<ShiPanEOrderStatusRespond> OrderResponses = mytrader.GetOrders();
            Console.WriteLine("我有{0}笔委托", OrderResponses.Count);

            //下单(限价委托)
            int Order_Num = mytrader.PlaceOrder("600256", "BUY", ShiPanEOrderPriceType.Limit, 12.56, 1000);
            if (Order_Num > 0)
                Console.WriteLine("下单成功，委托代码为{0}", Order_Num);
            else
                Console.WriteLine("下单失败，请重试。");

            //获取持仓
            List<ShiPanEStockPosition> AccountPositions = mytrader.GetPositions();
            Console.WriteLine("当前我有{0}笔持仓.", AccountPositions.Count);

            //获取可用资金
            Console.WriteLine("我的可用资金为{0}元.",mytrader.AccountAsset.CanUse);


            //Finishe line
            Console.WriteLine("程序结束");
            Console.ReadLine();
        }
    }
}
```